
Description
-----------
Track Host is  a simple module for Drupal that help site publisher to follow the step-by-step navigation , tracking "how" and for"how long" anonymous and registered users browse through your web site.

Filtering information from the access_log table (host field) this module gives to you tracking of the user (IP host) navigation.

The module provides:
- a list table of the recent hosts
- a detail table with the host navigation traking
- a side block from which you'll have access to the list and detail table

Since these are information usually read from the site administrator (and/or mantainer)
it is a good idea to limit the block to the admin/* pages



Requirements
------------

-- Drupal 4.7.x core


Installation
------------

1) copy the track_host directory into the modules directory

2) enable the 'track_host module' in drupal

3) edit admin/settings/track_host and change any other default setting depending your needs

4) enable the 'track_host block', probably to show up only on the admin/* pages

Instructions
------------

Screenshots
----
http://www.fagioli.biz/?q=track-host-module-drupal-snapshot



To do
-----
- get host name from IP
- get regional localization from IP
- get client time navigation from IP


Credit
------
Written by:
Augusto Fagioli
http://www.fagioli.biz




History
-------
(I haven't been keeping up with this sorry)


2007-04-03 Initial Release for Drupal 4.7

